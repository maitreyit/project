package com.example.demo;

import java.util.Arrays;
import java.util.Collection;
import java.util.Iterator;

public class MyCollection {
	
	public static void main(String[] args)
	{
		String str[]= {"One","Two","Three"};
		Collection collection=Arrays.asList(str);
		Iterator iterator=collection.iterator();
		while(iterator.hasNext())
		{
			String str1=(String)iterator.next();
			System.out.println(str1);
		}
	}

}
