package myproject.Map_HashMap1;


import java.rmi.StubNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {
	private static Scanner scanner=new Scanner(System.in);
    
    private static final Integer ONE = new Integer(1);
    private static int i=0;
    public static void main(String[] args) {
		/*
		 * // Set up testing data String name[] = { new String("Sang"), new
		 * String("Shin"), new String("Boston"), new String("Passion"), new
		 * String("Shin") };
		 * 
		 * // Create a HashMap object Map<String,Integer> m = new
		 * HashMap<String,Integer>();
		 * 
		 * // Initialize frequency table with testing data for (int i=0; i<name.length;
		 * i++) { // Check if the name is alredy in the map. Integer freq = (Integer)
		 * m.get(name[i]);
		 * 
		 * // If the name was not in the map already, // then set it to 1, otherwise
		 * increament it. m.put(name[i], (freq==null ? ONE : new Integer(freq.intValue()
		 * + 1))); }
		 * 
		 * // Display the size of the Map object System.out.println(m.size() +
		 * " distinct words detected:");
		 * 
		 * // Display Map object System.out.println("Display of the HashMap object = " +
		 * m);
		 */
    	
    	
    	StudentDashBoard dashBoard=new StudentDashBoard();
    	int choice=0;
    	do {
    		System.out.println("1: Create");
    		System.out.println("2: Display All");
    		System.out.println("3: Display By ID ");
    		System.out.println("4: Remove By ID");
    		System.out.println("0: Exit");
    		System.out.print("1: Choose Your Choice: ");
    		choice=scanner.nextInt();
    		switch (choice) {
			case 1:
				System.out.print("First Name: ");
				String fName=scanner.next();
				System.out.print("Last Name: ");
				String lName=scanner.next();
				System.out.print("Email: ");
				String email=scanner.next();
				dashBoard.createAndAddStudent(new Student(++i, fName, lName, email));
				System.out.println("Student Created Sucessfully...!");
				break;
			case 2:
				try {
					dashBoard.displayStudents();
				} catch (StudentNotFoundExeption e1) {
				
					System.out.println(e1.getMessage().toUpperCase());
				}
				break;
			case 3:
				System.out.println("Enter ID: ");
				try {
					Student student=dashBoard.getStudentById(scanner.nextInt());
					System.out.println(student);
					
				} catch (StudentNotFoundExeption e) {
					System.out.println(e.getMessage().toUpperCase());
				}
				
			
				break;
			case 4:
				System.out.println("Enter ID: ");
				try {
					dashBoard.removeStudentById(scanner.nextInt());
					
					
				} catch (StudentNotFoundExeption e) {
					System.out.println(e.getMessage().toUpperCase());
				}
				
			
				break;
			case 0:
				System.out.println("Bye....!");
				System.exit(0);

			default:
				break;
			}
			
		} while (choice !=0);
    	
    	
    	
    	
    	
    	
    	
    	
    }
    
}

