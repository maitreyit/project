package myproject.Sorting_NaturalOrder1;

import java.util.Comparator;

public class MyPersonComparator {
	
	public static Comparator<Person> personComparator()

	{
		return new Comparator<Person>() {

	

			public int compare(Person o1, Person o2) {
				
				  return (o1.getPersonAge()<o2.getPersonAge() ? 1 : (o1.getPersonAge()==o2.getPersonAge() ? 0 : -1));
				
			}
		};
	}

}
